package com.example.nycschool.repository;

import com.example.nycschool.model.SchoolSatData;
import com.example.nycschool.network.APIService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SchoolSatDataRepository {

    private Retrofit retrofit;

    @Inject
    public SchoolSatDataRepository(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public Observable<List<SchoolSatData>> getNYCSatDataList() {

        return retrofit.create(APIService.class)
                .getSatDataList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


}
