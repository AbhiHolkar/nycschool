/*
 * Developed by Abhinav Holkar on 16/03/19 10:14.
 * Last modified 16/03/19 10:14.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.network;

import com.example.nycschool.model.SchoolData;
import com.example.nycschool.model.SchoolSatData;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface APIService {

    String BASE_URL = "https://data.cityofnewyork.us/";

    @GET("resource/s3k6-pzi2.json")
    Observable<List<SchoolData>> getSchoolList();


    @GET("resource/f9bf-2cp4.json")
    Observable<List<SchoolSatData>> getSatDataList();


}
