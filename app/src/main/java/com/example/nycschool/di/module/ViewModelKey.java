/*
 * Developed by Abhinav Holkar on 16/03/19 10:00.
 * Last modified 16/03/19 10:00.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.di.module;

import android.arch.lifecycle.ViewModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import dagger.MapKey;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@MapKey
@interface ViewModelKey {
    Class<? extends ViewModel> value();
}
