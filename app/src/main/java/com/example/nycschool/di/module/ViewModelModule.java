/*
 * Developed by Abhinav Holkar on 16/03/19 10:02.
 * Last modified 16/03/19 10:02.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.di.module;

import android.arch.lifecycle.ViewModel;

import com.example.nycschool.viewmodel.SchoolListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SchoolListViewModel.class)
    abstract ViewModel viewModel(SchoolListViewModel schoolListViewModel);
}
