/*
 * Developed by Abhinav Holkar on 16/03/19 10:38.
 * Last modified 16/03/19 10:38.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.usecase;

import android.support.annotation.VisibleForTesting;

import com.example.nycschool.model.NYCSchool;
import com.example.nycschool.model.SchoolData;
import com.example.nycschool.model.SchoolSatData;
import com.example.nycschool.repository.SchoolListRepository;
import com.example.nycschool.repository.SchoolSatDataRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;


public class GetSchoolListUseCase {

    private SchoolListRepository schoolListRepository;
    private SchoolSatDataRepository schoolSatDataRepository;

    @Inject
    public GetSchoolListUseCase(SchoolListRepository schoolListRepository, SchoolSatDataRepository schoolSatDataRepository) {
        this.schoolListRepository = schoolListRepository;
        this.schoolSatDataRepository = schoolSatDataRepository;
    }

    @VisibleForTesting
    Observable<List<SchoolSatData>> getSchoolSatDataList() {
        return schoolSatDataRepository.getNYCSatDataList();
    }

    @VisibleForTesting
    Observable<List<SchoolData>> getNYCSchoolList() {
        return schoolListRepository.getNYCSchoolList();
    }

    public Observable<List<NYCSchool>> getNYCSchools() {

        Observable<List<SchoolData>> schoolDataList = getNYCSchoolList();
        Observable<List<SchoolSatData>> schoolSatDataList = getSchoolSatDataList();


        final Observable<Map<String, SchoolData>> schoolDataMap = schoolDataList.flatMap(new Function<List<SchoolData>, ObservableSource<Map<String, SchoolData>>>() {
            @Override
            public ObservableSource<Map<String, SchoolData>> apply(List<SchoolData> schoolDataList) throws Exception {
                final Map<String, SchoolData> schoolDataHashMap = new HashMap<>();
                for (SchoolData schoolData : schoolDataList) {
                    schoolDataHashMap.put(schoolData.getDbn(), schoolData);
                }
                return Observable.just(schoolDataHashMap);
            }
        });

        Observable<Map<String, SchoolSatData>> schoolSatDataMap = schoolSatDataList.flatMap(new Function<List<SchoolSatData>, ObservableSource<Map<String, SchoolSatData>>>() {
            @Override
            public ObservableSource<Map<String, SchoolSatData>> apply(List<SchoolSatData> schoolSatDataList) throws Exception {
                final Map<String, SchoolSatData> schoolDataHashMap = new HashMap<>();
                for (SchoolSatData schoolSatData : schoolSatDataList) {
                    schoolDataHashMap.put(schoolSatData.getDbn(), schoolSatData);

                }
                return Observable.just(schoolDataHashMap);
            }
        });


        return Observable.combineLatest(schoolDataMap, schoolSatDataMap, new BiFunction<Map<String, SchoolData>, Map<String, SchoolSatData>, List<NYCSchool>>() {
            @Override
            public List<NYCSchool> apply(Map<String, SchoolData> schoolDataHashMap, Map<String, SchoolSatData> stringSchoolSatDataMap) throws Exception {
                List<NYCSchool> nycSchoolList = new ArrayList<>();
                for (Map.Entry<String, SchoolData> entry : schoolDataHashMap.entrySet()) {
                    NYCSchool nycSchool = new NYCSchool();
                    nycSchool.setSchoolData(entry.getValue());
                    nycSchool.setSchoolSatData(stringSchoolSatDataMap.get(entry.getKey()));
                    nycSchoolList.add(nycSchool);

                }
                return nycSchoolList;
            }
        });


    }

}
