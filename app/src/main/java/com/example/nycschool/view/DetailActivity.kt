package com.example.nycschool.view

import android.os.Bundle
import com.example.nycschool.R
import com.example.nycschool.model.NYCSchool
import com.example.nycschool.utils.BundleConstants
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val nycSchool = intent.extras.getParcelable<NYCSchool>(BundleConstants.NYC_SCHOOL_BUNDLE)
        name.text = nycSchool.schoolData.schoolName
        sat_math_value.text = nycSchool.schoolSatData?.satMathAvgScore ?: getString(R.string.n_a_val)
        sat_reading_value.text = nycSchool.schoolSatData?.satCriticalReadingAvgScore ?: getString(R.string.n_a_val)
        sat_writing_value.text = nycSchool.schoolSatData?.satWritingAvgScore ?: getString(R.string.n_a_val)

    }

}
