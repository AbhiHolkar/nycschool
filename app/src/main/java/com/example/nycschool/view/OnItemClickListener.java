

/*
 * Developed by Abhinav Holkar on 16/03/19 08:34.
 * Last modified 16/03/19 08:34.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.view;

public interface OnItemClickListener<T> {
    void onItemClicked(T item);
}
