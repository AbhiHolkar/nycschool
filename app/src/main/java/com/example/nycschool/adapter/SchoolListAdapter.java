/*
 * Developed by Abhinav Holkar on 16/03/19 18:47.
 * Last modified 16/03/19 18:47.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nycschool.R;
import com.example.nycschool.model.NYCSchool;
import com.example.nycschool.view.OnItemClickListener;

import java.util.List;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolNameVH> {

    private Context context;
    private List<NYCSchool> nycSchoolList;
    private OnItemClickListener itemClickListener;

    public SchoolListAdapter(Context context, List<NYCSchool> nycSchoolList, OnItemClickListener itemClickListener) {
        this.context = context;
        this.nycSchoolList = nycSchoolList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public SchoolNameVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new SchoolNameVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolNameVH holder, int position) {
        final NYCSchool nycSchool = nycSchoolList.get(position);
        holder.name.setText(nycSchool.getSchoolData().getSchoolName());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClicked(nycSchool);
            }
        });

    }

    @Override
    public int getItemCount() {
        return nycSchoolList.isEmpty() ? 0 : nycSchoolList.size();
    }
}
