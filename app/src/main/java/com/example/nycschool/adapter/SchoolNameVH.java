/*
 * Developed by Abhinav Holkar on 16/03/19 18:44.
 * Last modified 16/03/19 18:44.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.nycschool.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SchoolNameVH extends RecyclerView.ViewHolder {

    @BindView(R.id.name)
    public TextView name;

    View view;

    public SchoolNameVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.view = itemView;
    }

    public View getItemView() {
        return view;
    }
}
