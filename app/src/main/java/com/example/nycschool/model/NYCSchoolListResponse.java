/*
 * Developed by Abhinav Holkar on 16/03/19 10:43.
 * Last modified 16/03/19 10:43.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.model;

import android.support.annotation.Nullable;

import java.util.List;

public class NYCSchoolListResponse {

    @Nullable
    private List<NYCSchool> nycSchoolList;

    @Nullable
    private Throwable error;

    public NYCSchoolListResponse(@Nullable List<NYCSchool> nycSchoolList, @Nullable Throwable error) {
        this.nycSchoolList = nycSchoolList;
        this.error = error;
    }


    @Nullable
    public List<NYCSchool> getNycSchoolList() {
        return nycSchoolList;
    }

    @Nullable
    public Throwable getError() {
        return error;
    }
}
