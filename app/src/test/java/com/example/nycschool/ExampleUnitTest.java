/*
 * Developed by Abhinav Holkar on 16/03/19 19:34.
 * Last modified 16/03/19 07:58.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}