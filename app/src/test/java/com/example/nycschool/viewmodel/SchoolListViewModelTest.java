package com.example.nycschool.viewmodel;


import com.example.nycschool.model.NYCSchool;
import com.example.nycschool.usecase.GetSchoolListUseCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.junit.Assert.assertNotNull;

public class SchoolListViewModelTest {

    @Mock
    private GetSchoolListUseCase getSchoolListUseCase;
    private SchoolListViewModel schoolListViewModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        schoolListViewModel = new SchoolListViewModel(getSchoolListUseCase);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getSchoolList() {

        //Given :
        TestObserver testObserver = new TestObserver<>();
        Observable<List<NYCSchool>> mockObservable = Mockito.mock(Observable.class);
        mockObservable.subscribe(testObserver);

        //when:
        Mockito.when(getSchoolListUseCase.getNYCSchools()).thenReturn(mockObservable);
        schoolListViewModel.getSchoolList();


        //Then:
        assertNotNull(getSchoolListUseCase);
        testObserver.assertNoErrors();
        testObserver.onComplete();
        testObserver.assertComplete();

    }

    @Test(expected = Throwable.class)
    public void getSchoolListWithException() throws Exception {

        //Given :
        TestObserver testObserver = new TestObserver<>();
        Throwable mockException = Mockito.mock(Throwable.class);
        Observable<List<NYCSchool>> mockObservable = Mockito.mock(Observable.class);
        mockObservable.subscribe(testObserver);

        //when:
        Mockito.when(getSchoolListUseCase.getNYCSchools()).thenThrow(mockException);
        schoolListViewModel.getSchoolList();


        //Then:
        assertNotNull(getSchoolListUseCase);
        testObserver.assertError(mockException);
        testObserver.onComplete();
        testObserver.assertComplete();

    }
}