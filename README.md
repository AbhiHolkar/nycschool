# NYCSchools

NYC Schools application displays School information (school name & SAT scores) for all schools in New York. App uses following end point
 - https://data.cityofnewyork.us/resource/s3k6-pzi2.json
 - https://data.cityofnewyork.us/resource/f9bf-2cp4.json
 
 App is mix of Kotlin and Java language. The first view displays the school name and on next screen SAT scores for selected school is displayed.

# Application Architecture.
- Application uses MVVM architecture & CLEAN architecture & principles of SOLID programming.
- Dagger 2 is used for Dependency Injection and ButterKnife for View Injections.
- Retrofit is used for Network layer.
- RxJava2 is used for Background/Asychronous operations like Network call/ response fetch and 30 secs refresh.
- GSON library is used for parsing of data
- Glide is used to fetch/resize images.
- Application uses Mockito for Junit test cases.
- Repository and Use case concept of Clean architecture makes code modular and testable.
- Recylcer view is used to display list of feeds

# Pre-Requiste
- Android studio . Java 1.8 & kotlin.

# Run/Test
- App has been tested on device S5
- App unit test covers Viewmodel and Usecase.

# Techincal Debt
- Add persistence to save response using any DB(Realm/ORM/SQLite)
- Add more test coverage.
- UI code on fragments

# Known bug
- TBD persistence
